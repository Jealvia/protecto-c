#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <sys/sysinfo.h>
#include <string.h>

#define FILENAME "example.txt"
#define FILENAME1 "pruebamenor.txt"
#define VALORPALABRA 1000

typedef struct conpalth {
    FILE * fp;
    int bandera;
    int contador;
}
conpalth_t;
pthread_mutex_t mutex;

conpalth_t contador_general;
char cadena[VALORPALABRA];
void contar_palabras() {
    while (contador_general.bandera) {
        pthread_mutex_lock( & mutex);
        if (fscanf(contador_general.fp, "%s", cadena) == 1) {
            contador_general.contador++;
        } else {
            contador_general.bandera = 0;
        }
        pthread_mutex_unlock( & mutex);
    }
}

conpalth_t inicializar_estructura() {
    contador_general.bandera = 1;
    contador_general.contador = 0;
    contador_general.fp = fopen(FILENAME1, "r");
}

int main(void) {
    //get_nprocs()
    clock_t tiempo_inicio, tiempo_final;
    double segundos;
    int procesadores=get_nprocs();
    inicializar_estructura();
    tiempo_inicio = clock();
    pthread_mutex_init( & mutex, NULL);
    printf("Procesadores %d \n",procesadores);
    pthread_t thread1, thread2, thread3,*lista=malloc(procesadores * sizeof(pthread_t*));
    for (int i = 0; i < procesadores; i++)
    {
        pthread_create( & lista[i], NULL,(void * ) & contar_palabras,NULL);
    }
    for (size_t i = 0; i < procesadores; i++)
    {
        pthread_join(lista[i], NULL);
    }

    tiempo_final = clock();

    segundos = (double)(tiempo_final - tiempo_inicio) / CLOCKS_PER_SEC; /*según que estes midiendo el tiempo en segundos es demasiado grande*/

    printf("%f\n", segundos);
    printf("Palabras: %d \n", contador_general.contador);


    return EXIT_SUCCESS;
}