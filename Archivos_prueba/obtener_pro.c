/**
  @file main.c
  @brief Archivo main de la primera parte del proyecto, shell que maneja comando cd y exit.
  @author Alvia Apráez Julio
  @date 20/11/2019, 28/11/2019

*/
#include <sys/wait.h>

#include <sys/types.h>

#include <unistd.h>

#include <stdlib.h>

#include <stdio.h>
#include <sys/sysinfo.h>
#include <string.h>


int proceso_general(char ** args) {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == 0) {
        printf ("Todo bien");
        int lol=execvp(args[0], args);
        if (lol) {
            perror("Error ejecutando operacion");
        }
        int primernumero;
        scanf("%d", &primernumero);
        printf ("La cadena leída es: %d \n", primernumero);
        exit(EXIT_FAILURE);
    } else if (pid < 0) {
        perror("Error haciendo fork");
    } else {
        do {
            waitpid(pid, & status, WUNTRACED);
        } while (!WIFEXITED(status) && !WIFSIGNALED(status));
    }

    return 1;
}
//nproc --all
int main(int argc, char ** argv) {
    printf("This system has %d processors configured and "
        "%d processors available.\n",
        get_nprocs_conf(), get_nprocs());
    return 0;
    /* char * linea;
    char ** args;
    int estado;
    int primernumero;
    char ** lista_retorno = malloc(64 * sizeof(char * ));
    lista_retorno[0]="nproc";
    lista_retorno[1]="--all";
    estado = proceso_general(lista_retorno); */
    /* scanf("%d", &primernumero);
    printf ("La cadena leída es: %d \n", primernumero); */
    /* FILE *stdout;
    char cadena [100];
    fgets (cadena, 100, stdin);
    printf ("La cadena leída es: %s \n", cadena); */
    return EXIT_SUCCESS;
}