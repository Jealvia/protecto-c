/* #include<stdio.h> 
#include<string.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<sys/wait.h> 
#include<regex.h>


int main(int argc, char *argv[]){
    char *cadena=(char*)malloc(500);
    char *cadena2=(char*)malloc(500);
    fgets(cadena,100,stdin);
    int len=strlen(cadena);
    strncpy(cadena2, cadena, len-1);
    int changed = chdir(cadena2);
    if(changed<0)
        printf("Directory not changed\n");
    else
    {
    	printf("Directory changed to Desktop\n");
        execlp("ls", "ls", "-ltr", NULL);
    }
} */
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//declaracion de funciones
int comando_cd(char **args);
int comando_exit(char **args);

//Lista de comandos a manejar
char *lista_comandos[] = {
  "cd",
  "exit"
};

//Lista de funciones se maneja el mismo orden de la anterior para facilidad de acceso
int (*builtin_func[]) (char **) = {
  &comando_cd,
  &comando_exit
};

//Retorna numero de comandos actuales
int numero_comandos() {
  return sizeof(lista_comandos) / sizeof(char *);
}

// implementacion de cd
int comando_cd(char **args)
{
  if (args[1] == NULL) {
    fprintf(stderr, "Se esperaba el argumento \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("Error al cambiar de directorio");
    }
    printf("Directorio cambiado exitosamente\n");
  }
  return 1;
}

//Implementacion de comando exit
int comando_exit(char **args)
{
  return 0;
}

//Generacion del proceso para ejecucion de los comandos
int proceso_general(char **args)
{
  pid_t pid;
  int status;

  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("Error ejecutando operacion");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    perror("Error haciendo fork");
  } else {
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}

//Se verifica si lo ingresado corresponde a los programas seleccionados y se asigna su respectica ejecucion
int manejo_de_comandos(char **args)
{
  int i;

  if (args[0] == NULL) {
    return 1;
  }
  //Validan si el comando pertenece a los listados 
  for (i = 0; i < numero_comandos(); i++) {
    if (strcmp(args[0], lista_comandos[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }
  
  return proceso_general(args);
}

#define BUFSIZE 1024
//Lectura de la linea ingresada
char *leer_linea(void)
{
  int bufsize = BUFSIZE;
  int posicion = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "Error asignando memoria al buffer\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    c = getchar();
    if (c == EOF) {
      exit(EXIT_SUCCESS);
    } else if (c == '\n') {
      buffer[posicion] = '\0';
      return buffer;
    } else {
      buffer[posicion] = c;
    }
    posicion++;

    if (posicion >= bufsize) {
      bufsize += BUFSIZE;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "Error asignando memoria al buffer\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

#define SPLIT_BUFSIZE 64
#define DELIMITADORES " \t\r\n\a"
//Split de linea tomando sus argumentos
char **split_linea(char *line)
{
  int bufsize = SPLIT_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token, **tokens_backup;

  if (!tokens) {
    fprintf(stderr, "Error asignando memoria al buffer\n");
    exit(EXIT_FAILURE);
  }

  token = strtok(line, DELIMITADORES);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += SPLIT_BUFSIZE;
      tokens_backup = tokens;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
		free(tokens_backup);
        fprintf(stderr, "Error asignando memoria al buffer\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, DELIMITADORES);
  }
  tokens[position] = NULL;
  return tokens;
}

#define MENSAJE "SO20190037sh> "
//Main
int main(int argc, char **argv)
{
  char *linea;
  char **args;
  int status;

  do {
    printf(MENSAJE);
    linea = leer_linea();
    args = split_linea(linea);
    status = manejo_de_comandos(args);
    free(linea);
    free(args);
  } while (status);

  return EXIT_SUCCESS;
}

