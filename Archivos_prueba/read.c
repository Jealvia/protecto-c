#include <pthread.h>
#include <sys/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define FILENAME "example.txt"

typedef struct calculo_lineas {
  int line_count;
  FILE * fp;
  ssize_t line_size;
  char * line_buf;
  size_t line_buf_size;
}
calc_lineas_t;
calc_lineas_t contador;
pthread_mutex_t mutex;

calc_lineas_t inicializar_estructura() {
  calc_lineas_t contador;
  contador.line_count = 1;
  contador.fp = fopen(FILENAME, "r");
  contador.line_buf = NULL;
  contador.line_buf_size = 0;
  return contador;
}

void imprimir() {
  printf("%d\n", contador.line_count);
}
void lectura() {
  while (contador.line_size >= 0) {
    pthread_mutex_lock( & mutex);
    if (contador.line_size >= 0) {
      contador.line_count++;
      contador.line_size = getline( & contador.line_buf, & contador.line_buf_size, contador.fp);
    }
    pthread_mutex_unlock( & mutex);
  }
}

int main(void) {
  clock_t tiempo_inicio, tiempo_final;
  double segundos;
  int procesadores=get_nprocs();
  printf("Procesadores %d \n",procesadores);
  tiempo_inicio = clock();

  pthread_mutex_init( & mutex, NULL);
  pthread_t *lista=malloc(procesadores * sizeof(pthread_t*));
  contador = inicializar_estructura();

  if (!contador.fp) {
    fprintf(stderr, "Error opening file '%s'\n", FILENAME);
    return EXIT_FAILURE;
  }

  contador.line_size = getline( & contador.line_buf, & contador.line_buf_size, contador.fp);
  for (int i = 0; i < procesadores; i++)
    {
      pthread_create( & lista[i], NULL, (void * ) & lectura,NULL );
    }
    for (size_t i = 0; i < procesadores; i++)
    {
        pthread_join(lista[i], NULL);
    }
  free(contador.line_buf);
  contador.line_buf = NULL;
  
  fclose(contador.fp);

  printf("Numero de lineas: %d \n", contador.line_count);

  tiempo_final = clock();

  segundos = (double)(tiempo_final-tiempo_inicio) / CLOCKS_PER_SEC;

  printf("%f \n",segundos);
  return EXIT_SUCCESS;
}