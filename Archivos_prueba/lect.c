#include <pthread.h>
#include <sys/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

int main(void) {
    FILE * fileptr;
    char * buffer,*buffer2;
    long filelen;

    fileptr = fopen("example.txt", "rb"); // Open the file in binary mode
    fseek(fileptr, 0, SEEK_END); // Jump to the end of the file
    filelen = ftell(fileptr); // Get the current byte offset in the file
    printf("Tamano: %ld\n",filelen);
    rewind(fileptr); // Jump back to the beginning of the file
    fseek(fileptr, 0, SEEK_SET); // Jump to the end of the file
    printf("%ld\n", filelen);
    buffer = (char * ) malloc((276 ) * sizeof(char)); // el tamaño del archivo Enough memory for file + \0
    buffer2 = (char * ) malloc((276 + 1) * sizeof(char)); // Enough memory for file + \0
    fread(buffer, (276 ) * sizeof(char), 1, fileptr); // Read in the entire file
    //printf("%s\n",buffer);
    printf("------------------\n");
    
    int size = 1;
    size_t i = 0;
    while (buffer[i] != '\0') {
        i++;
        if(buffer[i]=='\n'){
            size++;
        }
    }
    
    printf("------------------\n");
    printf("%d\n",size);
    fseek(fileptr, 0, SEEK_SET); // Jump to the end of the file
    fread(buffer2, (276 ) * sizeof(char), 1, fileptr); // Read in the entire file
    //printf("%s\n",buffer2);
    fclose(fileptr); // Close the file
}

/* void contar_palabras(void * ptr) {
    read2_t * my_pair = (read2_t * ) ptr;
    long inicio_lectura;
    inicio_lectura = ( * my_pair).inicio;
    char * buffer;
    if (( * my_pair).final) {
        int valor = contador.tam_general_buffer - inicio_lectura;
        buffer = (char * ) malloc((valor) * sizeof(char));
    } else {
        buffer = (char * ) malloc((contador.divisiones) * sizeof(char));
    }
    pthread_mutex_lock( & mutex);
    fseek(contador.fp, inicio_lectura, SEEK_SET);
    if (( * my_pair).final) {
        int valor = contador.tam_general_buffer - inicio_lectura;
        fread(buffer, (valor) * sizeof(char), 1, contador.fp);
    } else {
        fread(buffer, (contador.divisiones) * sizeof(char), 1, contador.fp);
    }
    pthread_mutex_unlock( & mutex);
    char *lista = strtok(buffer, DELIMITADORES);
    int position = 0;
    while (lista != NULL) {
        printf("Hilo %ld \n",inicio_lectura);
        printf("%s\n",lista);
        lista_retorno[position] = lista;
        position++;
        if (position >= tam_bufer) {
          tam_bufer += BUFER_SPLIT;
          lista_temporal = lista_retorno;
          lista_retorno = realloc(lista_retorno, tam_bufer * sizeof(char*));
          if (!lista_retorno) {
	    	free(lista_temporal);
            fprintf(stderr, "Error asignando memoria al buffer\n");
            exit(EXIT_FAILURE);
          }
        }
        lista = strtok(NULL, DELIMITADORES); 
    }
}
 */