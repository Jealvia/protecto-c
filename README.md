# Protecto-c

o Descripción general del diseño: 
Shell: En este apartado se definieron costantes, estructuras y funciones
twc: En este apartado se hizo uso de 3 estructuras:
contar_palabra_t: para el conteo de palabras.
calculo_lineas_t: utilizada para el calculo de numero de lineas.
parametro_hilo_t: contiene parametros que reciben los hilos para la funcion contar lineas.

o Especificación completa: 
Shell: Los comandos del sistema se manejan en una lista de funciones y comandos que permiten identificar si son comandos propios del sistema o se ejecutaran haciendo uso de execvp, en caso de que un comando no pertenezca a esta lista el programa hara un fork y en el proceso hijo haciendo uso de execvp se dara pie a la ejecucion de los comandos del sistema.
twc: En este apartado se hace uso de 3 estructuras que controlan la ejecucion de ambos programas, asi como funciones para el calculo de numero de lineas y conteo de palabras, y un main respectivo que controla estas funciones, asi como un main general que se encarga de manejar el inicio del sistemas con la validacion de los parametros de entrada.


o Errores o problemas conocidos: 
-En twc no controle tipos de archivos especiale.