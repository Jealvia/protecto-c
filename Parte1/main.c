/**
  @file main.c
  @brief Archivo main de la primera parte del proyecto, shell que maneja comando cd y exit.
  @author Alvia Apráez Julio
  @date 20/11/2019, 28/11/2019

*/
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
  Declaracion de constantes
*/
#define TAM_BUFER 1024
#define MENSAJE "SO20190037sh> "
#define BUFER_SPLIT 64
#define DELIMITADORES " \t\r\n\a"

/*
  Declaracion de funciones
*/
int comando_cd(char **args);
int comando_exit(char **args);

/*
  Lista de funciones se maneja el mismo orden de la anterior para facilidad de acceso
*/
int (*list_funciones[]) (char **) = {
  &comando_cd,
  &comando_exit
};

/*
  Lista de comandos a manejar
*/
char *lista_comandos[] = {
  "cd",
  "exit"
};

/**
  @brief Retorna numero de comandos actualmente 

  @returns Nos da como resultado el tamaño de la lista de comandos.
*/
int numero_comandos() {
  return sizeof(lista_comandos) / sizeof(char *);
}

/**
  @brief Implementacion de cd

  Recibe la direccion de la ruta donde se va a mover y haciendo uso de la llamada del sistema chdir que recibe
  el parametro y cambia de directorio, en caso de que la direccion no sea valida, se muestra mensaje con el error
  suscitado.

  @param args cadena de caracteres que contiene la ruta del directorio donde se va a mover la shell

  @returns retorna 1 en caso exitoso, caso contrario muestra por pantalla
            los errores por los cuales no se realizo la operacion.
*/
int comando_cd(char **args)
{
  if (args[1] == NULL) {
    fprintf(stderr, "Se esperaba el argumento \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("Error al cambiar de directorio");
    }else{
      printf("Directorio cambiado exitosamente\n");
    }
    
  }
  return 1;
}

/**
  @brief Implementacion de comando exit

  Cierra la shell, haciendo uso del

  @param args cadena de caracteres que contiene el comando a ejecutarse

  @returns retorna 0.
*/
int comando_exit(char **args)
{
  return 0;
}

/**
  @brief Generacion del proceso para ejecucion de los comandos

  Haciendo uso de fork se genera un proceso hijo donde se va a realizar la ejecucion del comando de la shell
  distinto a cd or exit, haciendo uso de execvp mientras el padres espera su salida.

  @param args cadena de caracteres que contiene el comando a ejecutarse

  @returns retorna 1 cuando su ejecucion fue exitosa, caso contrario muestra los errores y cierra la shell
*/
int proceso_general(char **args)
{
  pid_t pid;
  int status;

  pid = fork();
  if (pid == 0) {
    if (execvp(args[0], args) == -1) {
      perror("Error ejecutando operacion");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    perror("Error haciendo fork");
  } else {
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}

/**
  @brief Se verifica si lo ingresado corresponde a los programas seleccionados y se asigna su respectica ejecucion
  caso que no sean los comandos cd o exit se ejecutan los comandos mediante el uso de execvp.

  @param args cadena de caracteres que contiene el comando a ejecutarse

  @returns retorna 1 en caso de que los parametros sean nulos, caso contrario retorna la respectiva ejecucion de los comandos
  cd o exit, si no son algunos de estos entonces retorna la funcionalidad de manejo de proceso_general.
*/
int manejo_de_comandos(char **args)
{
  int i;

  if (args[0] == NULL) {
    return 1;
  }
  //Validan si el comando pertenece a los listados 
  for (i = 0; i < numero_comandos(); i++) {
    if (strcmp(args[0], lista_comandos[i]) == 0) {
      return (*list_funciones[i])(args);
    }
  }
  return proceso_general(args);
}

/**
  @brief Lectura de la linea ingresada

  Se separa un espacio en memoria para la lectura de la linea ingresada por el usuario, luego caracter por caracter
  se va llenando el buffer validando que no sea el fin de la linea o '\n', si el tamaño en el bufer no es el suficiente
  haciendo uso de realloc se mueve lo actual hacia un espacio del doble del tamaño actual

  @returns retorna un puntero a char que contiene los distintos parametros que recibiran las funciones,
  en la primera posicio el comando y en el resto los argumentos
*/
char *leer_linea()
{
  int bufsize = TAM_BUFER;
  int posicion = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "Error asignando memoria al buffer\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    c = getchar();
    if (c == EOF) {
      exit(EXIT_SUCCESS);
    } else if (c == '\n') {
      buffer[posicion] = '\0';
      return buffer;
    } else {
      buffer[posicion] = c;
    }
    posicion++;

    if (posicion >= bufsize) {
      bufsize += TAM_BUFER;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "Error reasignando memoria al buffer\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

/**
  @brief Split de linea tomando sus argumentos

  Recibe la linea ingresada por el ususario y realiza un split por los delimitadores declarados en la constante
  DELIMITADORES, haciendo uso de la funcion strtok, luego se itera esta lista y se almacena en la lista_retorno,
  en caso que el espacio en memoria de esta lista no es suficiente se realiza un realloc y se amplia este tamaño
  y se continua llenando hasta que sea null

  @param linea cadena de caracteres que fue escrita por el usuario

  @returns retorna lista_retorno, con las palabras escritas por el usuario.
*/
char **split_linea(char *linea)
{
  int tam_bufer = BUFER_SPLIT, position = 0;
  char **lista_retorno = malloc(tam_bufer * sizeof(char*));
  char *lista, **lista_temporal;

  if (!lista_retorno) {
    fprintf(stderr, "Error asignando memoria al buffer\n");
    exit(EXIT_FAILURE);
  }

  lista = strtok(linea, DELIMITADORES);
  while (lista != NULL) {
    lista_retorno[position] = lista;
    position++;

    if (position >= tam_bufer) {
      tam_bufer += BUFER_SPLIT;
      lista_temporal = lista_retorno;
      lista_retorno = realloc(lista_retorno, tam_bufer * sizeof(char*));
      if (!lista_retorno) {
		free(lista_temporal);
        fprintf(stderr, "Error asignando memoria al buffer\n");
        exit(EXIT_FAILURE);
      }
    }

    lista = strtok(NULL, DELIMITADORES);
  }
  lista_retorno[position] = NULL;
  return lista_retorno;
}


/**
  @brief Main general de la funcion

  Inicializa variables linea, que contendra la lectura de la linea, args que el la linea luego de ser tratada con el
  split por los distintos delimitadores, y estado que se encargara de manejar la finalizacion del programa, el comando
  exit controla esta salida.

  @param argc Numero de argunemtos.
  @param argv Vector con los argumentos.

  @returns retorna 1.
*/
int main(int argc, char **argv)
{
  char *linea;
  char **args;
  int estado;

  do {
    printf(MENSAJE);
    linea = leer_linea();
    args = split_linea(linea);
    estado = manejo_de_comandos(args);
    free(linea);
    free(args);
  } while (estado);

  return EXIT_SUCCESS;
}

