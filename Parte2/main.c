/**
  @file main.c
  @brief Archivo main de la segunda parte del proyecto, twc que se encarga de contar palabras asi como numero de lineas
  @author Alvia Apráez Julio
  @date 20/11/2019, 28/11/2019

*/
#include <pthread.h>
#include <sys/sysinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>

/**
 * Constantes del sistema
*/
#define VALORPALABRA 1000
#define PARAMETRO1 "twc"
#define PARAMETRO2 "-w"
#define PARAMETRO3 "-l"
#define PARAMETRO4 "-lw"
#define PARAMETRO5 "-wl"

/**
 * Estructura para contar palabras.
*/
typedef struct contar_palabra {
    FILE * fp;
    int bandera;
    int contador;
    int numero_procesadores;
}
contar_palabra_t;

/**
 * Estructura para calcular numero de lineas.
*/
typedef struct calculo_lineas {
    int line_count;
    FILE * fp;
    float divisiones;
    int numero_procesadores;
    int tam_general_buffer;
    int * valores;
}
calc_lineas_t;

/**
 * Estructura para el manejo de parametros que reciben los threads.
*/
typedef struct parametro_hilo {
    long inicio;
    int final;
}
parametro_hilo_t;

/**
 * Variable globales del sistema.
*/
calc_lineas_t contador;
contar_palabra_t contador_palabra;
pthread_mutex_t mutex,mutex_lector;
char cadena[VALORPALABRA];
char * nombre_archivo;

/**
  @brief Cuenta numero de palabras en un archivo.
        Maneja y controla toda la funcionalidad del conteo de palabras de un archivo,
        es un archivo main que hace uso de la estructura de datos como variable global
        para el manejo y control de condicion de carrera, incrementa el numero de palabras
        en la estructura contar_palabra_t en 1 por cada ocurrencia, asi como cambiar la bandera
        del ciclo while para que el resto de hilos no se ejecuten.

  @returns No retorna nada
*/
void contar_palabras() {
    while (contador_palabra.bandera) {
        pthread_mutex_lock( & mutex);
        if (fscanf(contador_palabra.fp, "%s", cadena) == 1) {
            contador_palabra.contador++;
        } else {
            contador_palabra.bandera = 0;
        }
        pthread_mutex_unlock( & mutex);
    }
}
/**
  @brief Inicializa la variable golbal contador_palabra.
        Se encarga de asignar los parametros iniciales a la estructura de contador_palabra que
        se encarga de la lectura del archivo, en caso de error cierra el programa por el 
        envio erroneo de la ruta.

  @returns No retorna nada
*/
void inicializar_estructura_contar_palabras() {
    contador_palabra.bandera = 1;
    contador_palabra.contador = 0;
    contador_palabra.fp = fopen(nombre_archivo, "r");
    if (!contador_palabra.fp) {
        fprintf(stderr, "Error abriendo archivo, ruta inexistente '%s'\n", nombre_archivo);
        exit(-1);
    }
    contador_palabra.numero_procesadores= get_nprocs();
}

/**
  @brief Inicializa una estructura calc_lineas_t para el uso de del contador de lineas.
        Se encarga de asignar los parametros iniciales a la estructura de calc_lineas_t que
        se encarga de la lectura del archivo, en caso de error cierra el programa por el 
        envio erroneo de la ruta.

  @returns calc_lineas_t estructura inicializada.
*/
calc_lineas_t inicializar_estructura_contar_linea() {
    calc_lineas_t contador;
    contador.numero_procesadores = get_nprocs();
    contador.line_count = 1;
    contador.fp = fopen(nombre_archivo, "rb");
    if (!contador.fp) {
        fprintf(stderr, "Error abriendo archivo, ruta inexistente '%s'\n", nombre_archivo);
        exit(-1);
    }
    fseek(contador.fp, 0, SEEK_END);
    contador.tam_general_buffer = ftell(contador.fp);
    rewind(contador.fp);
    contador.divisiones = contador.tam_general_buffer / contador.numero_procesadores;
    return contador;
}

/**
  @brief Funcion para contar numero de lineas
        Con la estructura parametro_hilo_t que nos indica el inicio de donde cada hilo debe
        empezar a leer, ademas de conocer si el hilo actual es el ultimo, en caso de que exista
        un buffer remanente entre las divisiones entre los hilos, esta le permitira tomar ese espacio
        de buffer para completar todo el archivo.

  @param * ptr recibe un puntero a void que contiene la estructura parametro_hilo_t, 
  que contiene parametros iniciales para la ejecucion del programa

  @returns No retorna nada.
*/
void contar_lineas(void * ptr) {
    parametro_hilo_t * estructura = (parametro_hilo_t * ) ptr;
    long inicio_lectura;
    inicio_lectura = ( * estructura).inicio;
    char * buffer;
    if (( * estructura).final) {
        int valor = contador.tam_general_buffer - inicio_lectura;
        buffer = (char * ) malloc((valor) * sizeof(char));
    } else {
        buffer = (char * ) malloc((contador.divisiones) * sizeof(char));
    }
    pthread_mutex_lock( & mutex_lector);
    fseek(contador.fp, inicio_lectura, SEEK_SET);
    if (( * estructura).final) {
        int valor = contador.tam_general_buffer - inicio_lectura;
        fread(buffer, (valor) * sizeof(char), 1, contador.fp);
    } else {
        fread(buffer, (contador.divisiones) * sizeof(char), 1, contador.fp);
    }
    pthread_mutex_unlock( & mutex_lector);
    int size = 0;
    size_t i = 0;
    while (buffer[i] != '\0') {
        if (buffer[i] == '\n') {
            size++;
        }
        i++;
    }
    pthread_mutex_lock( & mutex);
    contador.line_count = contador.line_count + size;
    pthread_mutex_unlock( & mutex);
}

/**
  @brief Funcion para el manejo del conteo de numero de lineas
        Se inicializa la estructura de contar linea, y se crean los n hilos necesarios
        que vienen dado por el numero de procesadores que tenga el hardware, luego se 
        asignan a estos hilos su respectiva estructura parametro_hilo_t para el manejo
        de la funcion contar_lineas, en caso de ser el ultimo hilo, este se le agrega un 
        1 a su valor de final dentro de la estructura para que se encargue del remanente
        buffer para el calculo de numero de lineas, imprimiendo su resultado por pantalla.

  @returns No retorna nada.
*/
void main_contar_lineas(){
    clock_t tiempo_inicio, tiempo_final;
    double segundos;

    contador = inicializar_estructura_contar_linea();
    pthread_mutex_init( & mutex, NULL);
    pthread_mutex_init( & mutex_lector, NULL);
    pthread_t * lista = malloc(contador.numero_procesadores * sizeof(pthread_t * ));

    tiempo_inicio = clock();
    for (int i = 0; i < contador.numero_procesadores; i++) {
        parametro_hilo_t * lol = malloc(sizeof(parametro_hilo_t));
        ( * lol).inicio = i * contador.divisiones;
        if ((i + 1) == contador.numero_procesadores) {
            ( * lol).final = 1;
        } else {
            ( * lol).final = 0;
        }
        pthread_create( & lista[i], NULL, (void * ) & contar_lineas, (void * ) lol);
    }
    for (size_t i = 0; i < contador.numero_procesadores; i++) {
        pthread_join(lista[i], NULL);
    }
    tiempo_final = clock();
    segundos = (double)(tiempo_final - tiempo_inicio) / CLOCKS_PER_SEC;
    fclose(contador.fp);
    printf("Numero de lineas: %d \n", contador.line_count);
    //printf("%f \n", segundos);
}

/**
  @brief Funcion para el manejo del conteo de palabras
        Se inicializa la estructura de contar linea contar_palabra_t, y se crean los n hilos necesarios
        que vienen dado por el numero de procesadores que tenga el hardware, cada hilo es asignado a la
        funcion contar_palabras, estos hilos son esperados para terminar la ejecucion

  @returns No retorna nada.
*/
void main_contar_palabras(){
    clock_t tiempo_inicio, tiempo_final;
    double segundos;
    inicializar_estructura_contar_palabras();
    tiempo_inicio = clock();
    pthread_mutex_init( & mutex, NULL);
    pthread_t *lista=malloc(contador_palabra.numero_procesadores * sizeof(pthread_t*));
    for (int i = 0; i < contador_palabra.numero_procesadores; i++)
    {
        pthread_create( & lista[i], NULL,(void * ) & contar_palabras,NULL);
    }
    for (size_t i = 0; i < contador_palabra.numero_procesadores; i++)
    {
        pthread_join(lista[i], NULL);
    }
    tiempo_final = clock();
    segundos = (double)(tiempo_final - tiempo_inicio) / CLOCKS_PER_SEC;

    //printf("%f\n", segundos);
    printf("Palabras totales: %d \n", contador_palabra.contador);
}

/**
  @brief Main general de la funcion

  Recibe los parametros necesarios para ejecutar el programa, si estos no se cumplen
  muestra un mensaje con la estructura deseada, si estos parametros se cumplen continua con la
  ejecucion.

  @param argc Numero de argunemtos.
  @param argv Vector con los argumentos.

  @returns retorna 1.
*/
int main(int argc, char **argv) {
    if(argc!=4) {
        printf("Error: La estructura de parametros es la siguiente: twc [-lw] [file ...]\n");
        printf("-w Calcula el numero de palabras en un archivo.\n");
        printf("-l Calcula el numero de lineas en un archivo.\n");
        exit(1);
    }
    if(strcmp(argv[1], PARAMETRO1)==0){
        if(strcmp(argv[2], PARAMETRO2)==0){
            nombre_archivo=argv[3];
            main_contar_palabras();
        }else if (strcmp(argv[2], PARAMETRO3)==0)
        {
            nombre_archivo=argv[3];
            main_contar_lineas();
        }else if( (strcmp(argv[2], PARAMETRO4)==0)||(strcmp(argv[2], PARAMETRO5)==0))
        {
            nombre_archivo=argv[3];
            main_contar_palabras();
            main_contar_lineas();
        }
        else{
            printf("Error: Parámetros incorrectos.\n");
            printf("Error: La estructura de parametros es la siguiente: twc [-lw] [file ...]\n");
            printf("-w Calcula el numero de palabras en un archivo.\n");
            printf("-l Calcula el numero de lineas en un archivo.\n");
        }
    }
    return 0;
}